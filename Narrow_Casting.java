package learn_TypeCasting;

public class Narrow_Casting {

	public static void main(String[] args) {
		
		double myDouble = 55.55;
		int myInt = (int) myDouble; // Manual casting: double to int
		
		System.out.println(myDouble);
		System.out.println(myInt);

		int maxScore = 500;
		int userScore = 423;

		float percentage = (float) userScore/maxScore *100f;

		System.out.println("User's percentage is " + percentage);
	}

}
