package learn_TypeCasting;

public class Widening_Casting {

	public static void main(String[] args) {
		
		int myInt = 10;
		double myDouble = myInt; // Automatic casting: int to double
		
		System.out.println(myInt);
		System.out.println(myDouble);

	}

}
